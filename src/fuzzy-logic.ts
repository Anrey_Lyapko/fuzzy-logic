import {ActivatedFuzzySet, Conclusion, Condition, FuzzySet, ObjectMap, Rule, UnionOfFuzzySets} from "./models";

let rules: Rule [] = [];
let numberOfOutputVariables: number = 0;

export function initRules(inputRules: Rule [], inputNumberOfOutputVariables: number): void {
    rules = inputRules;
    numberOfOutputVariables = inputNumberOfOutputVariables;
}

export function fuzzification(inputData: number []): number [] {
    let i = 0;
    const fuzzifiedData: number [] = [];
    rules.forEach((rule: Rule) => {
        rule.conditions.forEach((condition: Condition) => {
            const j = condition.variable.id;
            const term: FuzzySet = condition.term;
            fuzzifiedData[i] = term.value[inputData[j]];
            i++;
        });
    });
    return fuzzifiedData;
}

export function aggregation(fuzzifiedData: number []): number [] {
    let i = 0;
    let j = 0;
    let aggregationData: number [] = [];
    rules.forEach((rule: Rule) => {
        let truthOfConditions: number = 1;
        rule.conditions.forEach((condition: Condition) => {
            truthOfConditions = Math.min(truthOfConditions, fuzzifiedData[i]);
            i++;
        });
        aggregationData[j] = truthOfConditions;
        j++;
    });
    return aggregationData;
}

export function activation(aggregationData: number []): ActivatedFuzzySet [] {
    let i = 0;
    let activatedFuzzySets: ActivatedFuzzySet [] = [];
    let weights: number [] = [];
    rules.forEach((rule: Rule) => {
        rule.conclusions.forEach((conclusion: Conclusion) => {
            weights[i] = aggregationData[i] * conclusion.weight;
            let activatedFuzzySet: ActivatedFuzzySet = <ActivatedFuzzySet>conclusion.term;
            activatedFuzzySet.truthDegree = weights[i];
            activatedFuzzySets.push(activatedFuzzySet);
            i++;
        });
    });
    return activatedFuzzySets;
}

export function accumulation(activatedFuzzySets: ActivatedFuzzySet []): ObjectMap<UnionOfFuzzySets> {
    let unionsOfFuzzySets: ObjectMap<UnionOfFuzzySets> = {};

    rules.forEach((rule: Rule) => {
        rule.conclusions.forEach((conclusion: Conclusion) => {
            let id = conclusion.variable.id;
            unionsOfFuzzySets[id].fuzzySets.push(activatedFuzzySets[id]);
        });
    });

    return unionsOfFuzzySets;
}

export function defuzzification(unionsOfFuzzySets: ObjectMap<UnionOfFuzzySets>): number [] {
    let result: number [] = [];
    for (let i = 0; i < numberOfOutputVariables; i++) {
        let i1 = getMaxValue(unionsOfFuzzySets[i], i) / 0.5;
        let i2 = getMaxValue(unionsOfFuzzySets[i], i);
        result[i] = i1 / i2;
    }
    return result;
}

export function getMaxValue(unionOfFuzzySets: UnionOfFuzzySets, index: number): number {
    let result: number = 0;
    unionOfFuzzySets.fuzzySets.forEach((fuzzySet: FuzzySet) => {
        result = Math.max(result, fuzzySet.value[index]);
    });
    return result;
}