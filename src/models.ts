export class Rule {
    conclusions: Conclusion [];
    conditions: Condition [];

    constructor(conclusions: Conclusion [], conditions: Condition []) {
        this.conditions = conditions;
        this.conclusions = conclusions;
    }
}

export class Statement {
    term: FuzzySet;
    variable: Variable;

    constructor(term: FuzzySet, variable: Variable) {
        this.term = term;
        this.variable = variable;
    }
}

export class Conclusion extends Statement {
    weight: number;

    constructor(weight: number, term: FuzzySet, variable: Variable) {
        super(term, variable);
        this.weight = weight;
    }
}

export class Condition extends Statement {

}

export class Variable {
    id: number;
    terms: FuzzySet [];

    constructor(id: number, terms: FuzzySet []) {
        this.id = id;
        this.terms = terms;
    }
}

export class ObjectMap<T> {
    [key: number]: T;
}

export class FuzzySet {
    value: ObjectMap<number>;

    constructor(value: ObjectMap<number>) {
        this.value = value;
    }
}

export class ActivatedFuzzySet extends FuzzySet {
    truthDegree: number;

    constructor(truthDegree: number, value: ObjectMap<number>) {
        super(value);
        this.truthDegree = truthDegree;
    }
}

export class UnionOfFuzzySets {
    fuzzySets: FuzzySet [];

    constructor(fuzzySets: FuzzySet []) {
        this.fuzzySets = fuzzySets;
    }
}